from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy

from mealplans.models import Meal_Plan


# Create your views here.


class MealplansListView(LoginRequiredMixin, ListView):
    model = Meal_Plan
    template_name = "mealplans/list.html"
    context_object_name = "mealplans"
    paginate_by = 2

    def get_queryset(self):
        return Meal_Plan.objects.filter(author=self.request.user)


class MealplansCreateView(LoginRequiredMixin, CreateView):
    model = Meal_Plan
    template_name = "mealplans/new.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("mealplans_list")

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.author = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("mealplans_detail", pk=plan.id)


class MealplansDetailView(LoginRequiredMixin, DetailView):
    model = Meal_Plan
    template_name = "mealplans/detail.html"

    def get_queryset(self):
        return Meal_Plan.objects.filter(author=self.request.user)


class MealplansUpdateView(LoginRequiredMixin, UpdateView):
    model = Meal_Plan
    template_name = "mealplans/edit.html"
    fields = ["name", "recipes", "date"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class MealplansDeleteView(LoginRequiredMixin, DeleteView):
    model = Meal_Plan
    template_name = "mealplans/delete.html"
    success_url = reverse_lazy("mealplans_list")

    def get_queryset(self):
        return Meal_Plan.objects.filter(author=self.request.user)
