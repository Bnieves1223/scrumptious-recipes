from django.urls import path
from mealplans.views import (
    MealplansDeleteView,
    MealplansUpdateView,
    MealplansListView,
    MealplansCreateView,
    MealplansDetailView,
)


urlpatterns = [
    path("", MealplansListView.as_view(), name="mealplans_list"),
    path(
        "create/",
        MealplansCreateView.as_view(),
        name="mealplans_create",
    ),
    path(
        "<int:pk>/",
        MealplansDetailView.as_view(),
        name="mealplans_detail",
    ),
    path(
        "<int:pk>/edit/",
        MealplansUpdateView.as_view(),
        name="mealplans_edit",
    ),
    path(
        "<int:pk>/delete/",
        MealplansDeleteView.as_view(),
        name="mealplans_delete",
    ),
]
