# Generated by Django 4.0.3 on 2022-04-29 22:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mealplans', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='meal_plan',
            name='date',
            field=models.DateField(),
        ),
    ]
