# Generated by Django 4.0.3 on 2022-04-30 23:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0008_recipe_author'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShoppingItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('food_item', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='recipes.fooditem')),
            ],
        ),
    ]
